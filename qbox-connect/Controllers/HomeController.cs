﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevDefined.OAuth.Framework;
using DevDefined.OAuth.Consumer;
using System.Text;

namespace qbox_connect.Controllers
{
    public class HomeController : Controller
    {
        const string AppId = "79cf8e0eb1975b4044bb88eb5acf0af564c6";
        const string AppName = "QBox Connect Demo";

        const string consumerKey = "qyprdjPMmUxwiUcfZYpe9X279P6V4F";
        const string consumerSecret = "iDN7Alasy4yNJbkhSeTmg21uqcYC3zHNVqw6uVeX";
        const string requestUrl = "https://oauth.intuit.com/oauth/v1/get_request_token";
        const string userAuthorizeUrl = "https://appcenter.intuit.com/Connect/Begin";
        const string accessUrl = "https://oauth.intuit.com/oauth/v1/get_access_token";

        string callBackUrl => Url.Action("Access", null, null, Request.Url.Scheme);

        private OAuthConsumerContext _consumerContext = null;

        OAuthConsumerContext consumerContext
        {
            get
            {
                if (_consumerContext == null)
                {
                    _consumerContext = new OAuthConsumerContext
                    {
                        ConsumerKey = consumerKey,
                        ConsumerSecret = consumerSecret,
                        SignatureMethod = SignatureMethod.HmacSha1
                    };
                }
                return _consumerContext;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Grant()
        {
            var session = new OAuthSession(consumerContext, requestUrl, userAuthorizeUrl, accessUrl, callBackUrl);

            IToken requestToken = session.GetRequestToken();
            // save requestToken in this session
            HttpContext.Session.Add("requestToken", requestToken);

            string authorizeUrl = session.GetUserAuthorizationUrlForToken(requestToken, callBackUrl);
            return Redirect(authorizeUrl);
        }

        public ActionResult Access(string oauth_token, string oauth_verifier, string realmId, string dataSource)
        {
            var session = new OAuthSession(consumerContext, requestUrl, userAuthorizeUrl, accessUrl, callBackUrl);

            IToken requestToken = HttpContext.Session["requestToken"] as IToken;
            if (requestToken == null)
            {
                return HttpNotFound();
            }
            requestToken.Realm = realmId;

            var accessToken = session.ExchangeRequestTokenForAccessToken(requestToken, oauth_verifier);

            // demo api call to verify valid access_token
            var res = session.Request()
                .ForUrl("https://sandbox-quickbooks.api.intuit.com/v3/company/193514473420699/query?query=select%20%2A%20from%20Item&minorversion=4")
                .Get()
                .WithAcceptHeader("application/json")
                .ToString();


            // append any extra info you want to routeParams
            return Redirect(Url.Action("End", null,
                new
                {
                    oauth_token = accessToken.Token,
                    oauth_token_secret = accessToken.TokenSecret,
                    realm_id = realmId,
                    app_id = AppId,
                    app_name = AppName
                }, Request.Url.Scheme));
        }

        public ActionResult End(string oauth_token, string oauth_token_secret, string realmId)
        {
            return View();
        }
    }
}